/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>    // needed for cmocka
#include <sys/types.h> // needed for cmocka
#include <setjmp.h>    // needed for cmocka
#include <cmocka.h>
#include <stdlib.h>
#include <string.h>

#include "wifiSched.h"
#include "wifiSched_util.h"

static amxd_object_t schedule_object;
static amxd_object_t parent_object;
static amxd_object_t root_object;
static amxp_scheduler_t scheduler_object;

void dummy_callback(const char* const sig_name _UNUSED,
                    const amxc_var_t* const data _UNUSED,
                    void* const priv _UNUSED) {

    return;
}
static void initialize_structs() {
    memset(&schedule_object, 0, sizeof(amxd_object_t));
    memset(&parent_object, 0, sizeof(amxd_object_t));
    memset(&root_object, 0, sizeof(amxd_object_t));
    memset(&scheduler_object, 0, sizeof(amxp_scheduler_t));
}

static void test_update_scheduler_fails_if_schedule_is_NULL(void** state _UNUSED) {
    initialize_structs();

    int ret = wifiSched_util_updateScheduler(&root_object, &parent_object, NULL, &scheduler_object, dummy_callback);

    assert_int_equal(ret, EXIT_FAILURE);
}

static void test_update_scheduler_fails_if_parent_is_NULL(void** state _UNUSED) {
    initialize_structs();

    int ret = wifiSched_util_updateScheduler(&root_object, NULL, &schedule_object, &scheduler_object, dummy_callback);

    assert_int_equal(ret, EXIT_FAILURE);
}

static void test_update_scheduler_scheduler_is_initialized_if_NULL(void** state _UNUSED) {
    initialize_structs();
    parent_object.priv = NULL;
    amxp_scheduler_t* dummy_pointer = NULL;

    int ret = wifiSched_util_updateScheduler(&root_object, &parent_object, &schedule_object, dummy_pointer, dummy_callback);

    assert_int_equal(ret, EXIT_FAILURE);
    assert_ptr_not_equal(&scheduler_object, NULL);
    assert_ptr_not_equal((amxp_scheduler_t*) parent_object.priv, NULL);

    dummy_pointer = (amxp_scheduler_t*) parent_object.priv;
    amxp_scheduler_delete(&dummy_pointer);
}

static void test_update_scheduler_set_item_fails(void** state _UNUSED) {
    initialize_structs();

    int ret = wifiSched_util_updateScheduler(&root_object, &parent_object, &schedule_object, &scheduler_object, dummy_callback);

    assert_int_equal(ret, EXIT_FAILURE);
}

static void test_schedule_cleanup_when_action_is_NOT_destroy(void** state _UNUSED) {
    initialize_structs();
    amxd_action_t action = amxd_object_template;

    amxd_status_t status = _wifiSched_scheduleCleanup_odf(NULL, NULL, action, NULL, NULL, NULL);

    assert_int_equal(status, amxd_status_function_not_implemented);
}

static void test_schedule_cleanup_when_object_is_NOT_template(void** state _UNUSED) {
    initialize_structs();
    amxd_object_t object;
    amxd_action_t action = action_object_destroy;
    object.type = amxd_object_singleton;

    amxd_status_t status = _wifiSched_scheduleCleanup_odf(&object, NULL, action, NULL, NULL, NULL);

    assert_int_equal(status, amxd_status_invalid_type);
}

static void test_schedule_cleanup_succeeds(void** state _UNUSED) {
    initialize_structs();
    amxd_object_t object;
    amxd_action_t action = action_object_destroy;
    amxp_scheduler_t* dummyScheduler;

    amxp_scheduler_new(&dummyScheduler);
    object.type = amxd_object_template;
    object.priv = dummyScheduler;

    amxd_status_t status = _wifiSched_scheduleCleanup_odf(&object, NULL, action, NULL, NULL, NULL);

    assert_int_equal(status, amxd_status_ok);
    assert_ptr_equal(object.priv, NULL);
}


int main(int argc _UNUSED, char* argv[] _UNUSED) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_update_scheduler_fails_if_schedule_is_NULL),
        cmocka_unit_test(test_update_scheduler_fails_if_parent_is_NULL),
        cmocka_unit_test(test_update_scheduler_scheduler_is_initialized_if_NULL),
        cmocka_unit_test(test_update_scheduler_set_item_fails),
        cmocka_unit_test(test_schedule_cleanup_when_action_is_NOT_destroy),
        cmocka_unit_test(test_schedule_cleanup_when_object_is_NOT_template),
        cmocka_unit_test(test_schedule_cleanup_succeeds),
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);
    return rc;
}