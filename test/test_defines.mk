ifndef STAGINGDIR
	$(error "STAGINGDIR not defined")
endif

MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
SUT_DIR = $(dir $(MKFILE_PATH))/..
$(info $$SUT_DIR is [${SUT_DIR}])

SUT_OBJECTS = \
	$(patsubst %.c, %.o, $(wildcard $(SUT_DIR)/test/testHelper/*.c))

INCDIRS = $(INCDIR_PRIV) $(if $(STAGINGDIR), $(STAGINGDIR)/include) $(if $(STAGINGDIR), $(STAGINGDIR)/usr/include)
PKGCONFDIR = $(STAGINGDIR)/usr/lib/pkgconfig

CFLAGS += -I$(SUT_DIR)/include \
		  -I$(SUT_DIR)/include_priv \
		  -I$(STAGINGDIR)/usr/include \
		  -std=gnu99 -g3 \
		  -Wextra -Wall -Werror \
		  $(addprefix -I ,$(INCDIRS)) \
		  $(shell PKG_CONFIG_PATH=$(PKGCONFDIR) pkg-config --define-prefix --cflags sahtrace pcb cmocka openssl test-toolbox)
LDFLAGS += -L$(STAGINGDIR)/lib \
		   -L$(STAGINGDIR)/usr/lib \
		   -L$(STAGINGDIR)/usr/lib/amx/wifi-scheduler \
		   -Wl,-rpath,$(STAGINGDIR)/lib \
		   -Wl,-rpath,$(STAGINGDIR)/usr/lib \
		   -Wl,-rpath,$(STAGINGDIR)/usr/lib/amx/wifi-scheduler \
		   $(shell PKG_CONFIG_PATH=$(PKGCONFDIR) pkg-config --define-prefix --libs sahtrace pcb cmocka openssl test-toolbox) \
		   -lamxb -lamxc -lamxd -lamxo -lamxp -lamxj\
		   -l:wifi-scheduler.so \
		   -L$(SUT_DIR)/src \
		   -L$(SUT_DIR)/src/Plugin \
		   -lm \
