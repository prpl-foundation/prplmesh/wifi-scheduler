#!/bin/sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}
name="wifi-scheduler"
MAX_SIGTERM_RETRIES=3

kill_process()
{
    # kill process with PID == $1
    # try a SIGTERM for MAX_SIGTERM_RETRIES tries, then SIGKILL
    # SIGTERM : 15 ; SIGKILL : 9

    exit_condition=false
    echo "killing PID" $1
    tries=0
    max_tries=$MAX_SIGTERM_RETRIES
    kill -0 $1 2>/dev/null
    while [[ $? -eq 0 && $((tries++)) -lt $max_tries ]] ; do
        # try sigterm : 15 first
        kill -SIGTERM $1 && sleep 1 && kill -0 $1 2>/dev/null
    done
    # if still running, try sigkill
    kill -0 $1 2>/dev/null && kill -SIGKILL $1 && echo "killed with sigkill"

    # kill -0 returns true if PID is running and we can send signals to it
    # && command linking executes the next if previous command returns true
}

case $1 in
    start|boot)
        mkdir -p /var/lib/${name}
        touch /var/lib/${name}/${name}_config.odl
        amxrt /etc/amx/${name}/${name}.odl &
        ;;
    stop|shutdown)
        if [ -f /var/run/${name}.pid ]; then
            echo "stopping ${name}"
            kill_process `cat /var/run/${name}.pid`
        fi
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        ubus-cli "WiFiScheduler.?"
        ;;
    log)
        echo "log ${name}"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|restart|debuginfo|log]"
        ;;
esac
