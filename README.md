# WiFi Scheduler
Provides a mechanism to enable/disable WiFi on a week-based schedule

## Usage
### Global WiFi scheduler
- The path to the data model object managing the global enable/disable functionalities indicated by WiFiScheduler.GlobalTargetConfig  (default = PrplMesh.Network)
- Create a new instance of WiFiScheduler.Network.Schedule or modify an existing one
- Fill the 'Day' parameter of the Schedule instance. Valid values are: 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'
- Fill the 'StartTime' parameter of the Schedule instance in the hh:mm format
- Fill the 'Duration' parameter of the Schedule instance in seconds
- Set the 'Enable' parameter of the Schedule instance to 1

Example 1: Create a new schedule that enables WiFi on Mondays at 4:00 pm for 1 hour (3600s) 
```
WiFiScheduler.Network.Schedule.+{Enable=1, StartTime=16:00, Duration=3600, Day=monday}
```

Example 2: Create a new disabled schedule to activate WiFi on Saturdays at 6:00 am for a whole day
```
WiFiScheduler.Network.Schedule.+{Enable=0, StartTime=06:00, Duration=86400, Day=saturday}
```

### Group WiFi scheduler
- The path to the data model object managing the group's enable/disable functionality is indicated by WiFiScheduler.GroupTargetConfig (default = PrplMesh.Network.Group)
- Create a new instance of WiFiScheduler.Network.Group or modify an existing one
- Set the 'Alias' parameter of the Group to match the name of the Group for which the schedule will apply
- Create a new instance of WiFiScheduler.Group.Schedule or modify an existing one
- Fill the 'Day' parameter of the Group Schedule instance. Valid values are: 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'
- Fill the 'StartTime' parameter of the Group Schedule instance in the hh:mm format
- Fill the 'Duration' parameter of the Group Schedule instance in seconds
- Set the 'Enable' parameter of the Group Schedule instance to 1

- WiFi scheduler enable/disable WiFi either through Enable parameter when using "Parameter" in EnableMethod or through setEnable api when using "Function" in EnableMethod

Example 2: Create a new schedule that enables WiFi for group 'lan' on Mondays at 4:00 pm for 1 hour (3600s)
```
WiFiScheduler.Group+{Alias=lan}
WiFiScheduler.Group.1.Schedule.+{Enable=1, StartTime=16:00, Duration=3600, Day=monday}

```

Example 3: Create a new schedule that enables WiFi for group 'br-lan' on Mondays at 4:00 pm for 1 hour (3600s)
when "Function" is used 'Alias' parameter of the Group need to match the name of the Bridge, This is currently not supported by PrplMesh.
```
WiFiScheduler.Group+{Alias=br-lan}
WiFiScheduler.Group.1.Schedule.+{Enable=1, StartTime=16:00, Duration=3600, Day=monday}

```


### Disabling all schedulers
The WiFiScheduler.Enable flag to can be used to modify the status of all schedules in one go:
- When set to 1, the current enabled/disabled state of existing schedules will not be modified
- When set to 0, all existing schedules will be disabled

## Additional notes
### wifi-scheduler limitations
The actual enable/disable behavior depends on several factors and conditions that the WiFi Scheduler cannot override. For instance, a Group will not be enabled if the WiFi is globally disabled.
### Behavior on boot
Schedules and their parameters are persistent across reboots. They're updated and activated during the component's initialization.
### Behavior on schedule addition
A newly added schedule will be ready to be triggered as soon as all of its parameters are valid and its Enable parameter is set to 1.
### Behavior on schedule removal
If a removed scheduler is active, a stop signal will be emitted, effectively triggering the same behavior as if its duration had expired.
 

### Overlapping schedules
When one or more active schedules overlap, the WiFi will remain enabled while at least one schedule remains active as shown in the following timing diagram:
![Timing diagram](doc/images/timingDiagram.png "Timing diagram")

### Time synchronization
For WiFiScheduler to apply configured WiFi schedules correctly the time synchronization is required i.e. Time.Status="Synchronized". Time.Status="Synchronized" should happen automatically over NTP with a valid WAN connectivity. No manual configuration should be required as default system configuration is embedded within the software. Hence ensure a valid WAN connection. 
