/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_PRIV_WIFISCHED_H_
#define SRC_INCLUDE_PRIV_WIFISCHED_H_
#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxp/amxp_scheduler.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <amxo/amxo.h>
#include <amxb/amxb_types.h>
#include <amxb/amxb.h>

#include "swl/swl_assert.h"
#include "swl/swl_string.h"
#include "swl/swl_common_conversion.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#define MAX_32BIT_CHARS 10

// Avoid compiler warnings
#define _UNUSED_(x) (void) x
#define _UNUSED __attribute__((unused))

typedef enum  {
    SCHED_REASON_START = 0,
    SCHED_REASON_STOP,
    SCHED_REASON_UNKNOWN,
    SCHED_REASON_MAX,
} wifiSched_scheduleReason_e;

typedef enum  {
    WIFI_SCHED_ENABLE_METHOD_PARAM,
    WIFI_SCHED_ENABLE_METHOD_FUNCT,
    WIFI_SCHED_ENABLE_METHOD_UNKNOWN,
    WIFI_SCHED_ENABLE_METHOD_MAX,
} wifiSched_enableMethod_e;

amxd_status_t _wifiSched_scheduleCleanup_odf(amxd_object_t* object,
                                             amxd_param_t* param,
                                             amxd_action_t reason,
                                             const amxc_var_t* const args,
                                             amxc_var_t* const retval,
                                             void* priv);

wifiSched_scheduleReason_e s_getReasonFromString(const char* reason);
wifiSched_enableMethod_e s_getEnableMethodFromString(char* enableMethod);

amxd_dm_t* wifiSched_getDataModel(void);
amxc_var_t* wifiSched_getGroupHashTable(void);
unsigned int* wifiSched_getActiveSchedulers(void);
amxb_bus_ctx_t* wifiSched_getTargetBusContext(void);

#endif /* SRC_INCLUDE_PRIV_WIFISCHED_H_ */
