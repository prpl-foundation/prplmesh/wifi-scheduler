/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "wifiSched.h"
#include "wifiSched_util.h"
#include "wifiSched_group.h"
#include <limits.h>

#define ME "wifSGrp"

/*
 * Writes an enable value to the data model of a Group
 * Returns EXIT_SUCCESS if succesful
 */
static int s_writeGroupEnable(amxd_object_t* schedule, const char* reason) {
    //Get an instance from the parent object
    amxd_object_t* groupInstance = amxd_object_get_parent(schedule);
    ASSERT_NOT_NULL(groupInstance, EXIT_FAILURE, ME, "Failed obtaining the parent of object 'schedule'");

    char* groupNameCstr = amxd_object_get_cstring_t(groupInstance, "Alias", NULL);
    ASSERT_NOT_NULL(groupNameCstr, EXIT_FAILURE, ME, "Failed obtaining parameter 'Alias' from Group object");
    char groupNameArr[swl_str_len(groupNameCstr) + 1];
    swl_str_copy(groupNameArr, sizeof(groupNameArr), groupNameCstr);
    free(groupNameCstr);

    bool enableValue = true;
    unsigned int index = amxd_object_get_index(groupInstance);
    ASSERT_NOT_EQUALS(index, 0, EXIT_FAILURE, ME, "Error getting index of group %s", groupNameArr);

    char indexstr[MAX_32BIT_CHARS];
    snprintf(indexstr, MAX_32BIT_CHARS, "%d", index);
    amxc_var_t* activeSchedulersVar = amxc_var_get_key(wifiSched_getGroupHashTable(), indexstr, AMXC_VAR_FLAG_COPY);
    ASSERT_NOT_NULL(activeSchedulersVar, EXIT_FAILURE, ME, "Failed obtaining requested variant");
    unsigned int activeSchedulers = amxc_var_constcast(uint32_t, activeSchedulersVar);

    int retc = 0;
    switch(s_getReasonFromString(reason)) {
    case SCHED_REASON_START:
        enableValue = true;
        activeSchedulers = (activeSchedulers == UINT_MAX) ? UINT_MAX : activeSchedulers + 1;
        amxc_var_set(uint32_t, activeSchedulersVar, activeSchedulers);
        retc = amxc_var_set_key(wifiSched_getGroupHashTable(), indexstr, activeSchedulersVar, AMXC_VAR_FLAG_UPDATE);
        ASSERT_EQUALS(retc, 0, EXIT_FAILURE, ME, "Failed writing data to variant");
        break;
    case SCHED_REASON_STOP:
        activeSchedulers = (activeSchedulers == 0) ? 0 : activeSchedulers - 1;
        enableValue = false || activeSchedulers;
        amxc_var_set(uint32_t, activeSchedulersVar, activeSchedulers);
        retc = amxc_var_set_key(wifiSched_getGroupHashTable(), indexstr, activeSchedulersVar, AMXC_VAR_FLAG_UPDATE);
        ASSERT_EQUALS(retc, 0, EXIT_FAILURE, ME, "Failed writing data to variant");
        break;
    default:
        break;
    }

    amxc_var_t groupTargetConfigVar;
    amxc_var_init(&groupTargetConfigVar);

    amxd_object_t* wifiSchedulerObject = amxd_object_get_parent(amxd_object_get_parent(groupInstance));
    ASSERT_NOT_NULL(wifiSchedulerObject, EXIT_FAILURE, ME, "Failed WiFiScheduler object");

    amxd_status_t ret = amxd_object_get_param(wifiSchedulerObject, "GroupTargetConfig", &groupTargetConfigVar);
    ASSERT_EQUALS(ret, amxd_status_ok, EXIT_FAILURE, ME, "Failed obtaining parameter 'GroupTargetConfig' from object");

    char* enableMethod = amxd_object_get_cstring_t(wifiSchedulerObject, "EnableMethod", &ret);
    ASSERT_EQUALS(ret, amxd_status_ok, EXIT_FAILURE, ME, "Failed obtaining parameter 'EnableMethod' from object");

    const char* matchPattern = wifiSched_util_getSearchPattern(enableMethod);
    char suffix[swl_str_len(matchPattern) + swl_str_len(groupNameArr) + 1];
    suffix[0] = 0;
    swl_str_catFormat(suffix, sizeof(suffix), matchPattern, groupNameArr);

    char* groupConfigPath = amxc_var_dyncast(cstring_t, &groupTargetConfigVar);
    int searchPathSize = swl_str_len(groupConfigPath) + swl_str_len(suffix) + 1;
    char searchPath[searchPathSize + 1];
    searchPath[0] = 0;
    swl_str_copy(searchPath, searchPathSize, groupConfigPath);
    swl_str_cat(searchPath, searchPathSize, suffix);
    free(groupConfigPath);

    SAH_TRACEZ_INFO(ME, "Querying %s", searchPath);

    ret = wifiSched_util_writeToDataModel(searchPath, enableValue, s_getEnableMethodFromString(enableMethod), wifiSched_getTargetBusContext());
    if(ret != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed setting %s to %i", searchPath, enableValue);
    }

    amxc_var_clean(&groupTargetConfigVar);
    free(enableMethod);
    return EXIT_SUCCESS;
}

void wifiSched_group_scheduleTriggered_cbf(const char* const sig_name _UNUSED,
                                           const amxc_var_t* const data,
                                           void* const priv) {
    amxd_object_t* schedule = (amxd_object_t*) priv;
    char* path = amxd_object_get_path(schedule, 0);
    const char* reason = GET_CHAR(data, "reason");
    const char* id = GET_CHAR(data, "id");
    amxc_ts_t now;
    char time[40];

    amxc_ts_now(&now);
    amxc_ts_to_local(&now);
    amxc_ts_format_precision(&now, time, 40, 0);

    SAH_TRACEZ_INFO(ME, "'%s' of schedule '%s' called with reason '%s' at %s", id, path, reason, time);
    s_writeGroupEnable(schedule, reason);
}

static void s_getGroupKey(char* dest, const amxc_var_t* const eventData) {
    uint32_t index = GET_UINT32(eventData, "index");
    snprintf(dest, MAX_32BIT_CHARS, "%d", index);
}

void _wifiSched_group_scheduleAdded_ehf(const char* const event_name _UNUSED,
                                        const amxc_var_t* const event_data,
                                        void* const priv _UNUSED) {
    amxd_object_t* schedules = amxd_dm_signal_get_object(wifiSched_getDataModel(), event_data);
    uint32_t index = GET_UINT32(event_data, "index");
    amxd_object_t* schedule = amxd_object_get_instance(schedules, NULL, index);

    amxd_object_t* parent = amxd_object_get_parent(schedule);
    amxp_scheduler_t* scheduler = (amxp_scheduler_t*) parent->priv;
    amxd_object_t* rootObject = amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(parent)));
    wifiSched_util_updateScheduler(rootObject, parent, schedule, scheduler, wifiSched_group_scheduleTriggered_cbf);
}

void _wifiSched_group_scheduleChanged_ehf(const char* const event_name _UNUSED,
                                          const amxc_var_t* const event_data,
                                          void* const priv _UNUSED) {
    amxd_object_t* schedule = amxd_dm_signal_get_object(wifiSched_getDataModel(), event_data);
    amxd_object_t* parent = amxd_object_get_parent(schedule);
    amxp_scheduler_t* scheduler = (amxp_scheduler_t*) parent->priv;
    amxd_object_t* rootObject = amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(parent)));
    wifiSched_util_updateScheduler(rootObject, parent, schedule, scheduler, wifiSched_group_scheduleTriggered_cbf);
}

void _wifiSched_group_createGroup_ehf(const char* const event_name _UNUSED,
                                      const amxc_var_t* const event_data _UNUSED,
                                      void* const priv _UNUSED) {

    char key[MAX_32BIT_CHARS];
    s_getGroupKey(key, event_data);
    amxc_var_add_key(uint32_t, wifiSched_getGroupHashTable(), key, 0);
}

void _wifiSched_group_deleteGroup_ehf(const char* const event_name _UNUSED,
                                      const amxc_var_t* const event_data _UNUSED,
                                      void* const priv _UNUSED) {
    char key[MAX_32BIT_CHARS];
    s_getGroupKey(key, event_data);
    amxc_var_take_key(wifiSched_getGroupHashTable(), key);
}

void _wifiSched_group_scheduleDeleted_ehf(const char* const event_name _UNUSED,
                                          const amxc_var_t* const event_data,
                                          void* const priv _UNUSED) {
    amxd_object_t* schedules = amxd_dm_signal_get_object(wifiSched_getDataModel(), event_data);
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxp_scheduler_t* scheduler = NULL;

    ASSERT_NOT_NULL(schedules, , ME, "Failed getting schedules");
    scheduler = (amxp_scheduler_t*) schedules->priv;

    amxp_scheduler_remove_item(scheduler, GET_CHAR(params, "Alias"));
}
