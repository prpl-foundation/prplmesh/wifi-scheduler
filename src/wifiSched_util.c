/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>

#include <amxc/amxc.h>
#include "debug/sahtrace.h"
#include "wifiSched.h"
#include "wifiSched_util.h"

#define ME "wischut"

#define SIMPLE_MATCH_PATTERN ".%s."
#define GROUP_MATCH_PATTERN ".[Name == '%s']."

const char* wifiSched_util_getSearchPattern(char* enableMethod) {
    switch(s_getEnableMethodFromString(enableMethod)) {
    case WIFI_SCHED_ENABLE_METHOD_FUNCT:
        return SIMPLE_MATCH_PATTERN;
    default:
        return GROUP_MATCH_PATTERN;
    }
}

int wifiSched_util_updateScheduler(amxd_object_t* rootObject,
                                   amxd_object_t* parent,
                                   amxd_object_t* schedule,
                                   amxp_scheduler_t* scheduler,
                                   amxp_slot_fn_t triggeredCallbackFn) {

    ASSERT_NOT_NULL(schedule, EXIT_FAILURE, ME, "schedule is NULL");
    ASSERT_NOT_NULL(parent, EXIT_FAILURE, ME, "parent is NULL");
    ASSERT_NOT_NULL(triggeredCallbackFn, EXIT_FAILURE, ME, "triggeredCallbackFn is NULL");
    ASSERT_NOT_NULL(rootObject, EXIT_FAILURE, ME, "rootObject is NULL");

    amxc_var_t params;

    amxc_var_init(&params);

    if(scheduler == NULL) {
        amxp_scheduler_new(&scheduler);
        amxp_scheduler_use_local_time(scheduler, true);
        parent->priv = scheduler;
    }

    amxd_object_get_params(schedule, &params, amxd_dm_access_public);

    amxp_scheduler_disconnect(scheduler, GET_CHAR(&params, "Alias"), triggeredCallbackFn);
    if(amxp_scheduler_set_weekly_item(scheduler,
                                      GET_CHAR(&params, "Alias"),
                                      GET_CHAR(&params, "StartTime"),
                                      GET_CHAR(&params, "Day"),
                                      GET_UINT32(&params, "Duration"))) {

        SAH_TRACEZ_ERROR(ME, "Failed to set scheduler item. 'StartTime', 'Day' or 'Duration' may be invalid");

        amxc_var_clean(&params);
        return EXIT_FAILURE;
    }

    char* path = amxd_object_get_path(parent, AMXD_OBJECT_NAMED);
    SAH_TRACEZ_INFO(ME, "Schedule '%s' for '%s' created or updated", GET_CHAR(&params, "Alias"), path);

    bool globalEnable = amxd_object_get_value(bool, rootObject, "Enable", NULL);
    bool scheduleEnable = GET_BOOL(&params, "Enable");
    bool running = globalEnable && scheduleEnable;
    if(running) {
        amxp_scheduler_connect(scheduler,
                               GET_CHAR(&params, "Alias"),
                               triggeredCallbackFn,
                               parent);
    } else {
        SAH_TRACEZ_WARNING(ME, "Schedule '%s' for '%s' is disabled", GET_CHAR(&params, "Alias"), path);
    }

    amxc_var_t runningVariant;
    amxc_var_init(&runningVariant);
    amxc_var_set(bool, &runningVariant, running);
    amxd_object_set_param(schedule, "Running", &runningVariant);
    amxc_var_clean(&runningVariant);

    free(path);
    amxc_var_clean(&params);
    return EXIT_SUCCESS;
}

int wifiSched_util_writeToDataModel(char* searchPath, bool enableValue, wifiSched_enableMethod_e enableMethod, amxb_bus_ctx_t* busCtx) {

    amxc_var_t modifiedValues;
    amxc_var_init(&modifiedValues);
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    ASSERT_NOT_NULL(busCtx, 1, ME, "Failed commmunicating to bus system");

    int ret = 0;
    switch(enableMethod) {
    case WIFI_SCHED_ENABLE_METHOD_PARAM:
        amxc_var_add_key(bool, &args, "Enable", enableValue);
        ret = amxb_set(busCtx, searchPath, &args, &modifiedValues, 5);
        break;
    case WIFI_SCHED_ENABLE_METHOD_FUNCT:
        amxc_var_add_key(bool, &args, "enabled", enableValue);
        amxc_var_add_key(cstring_t, &args, "reason", "wifi-scheduler");
        ret = amxb_call(busCtx, searchPath, "setEnabled", &args, &modifiedValues, 5);
        break;
    default:
        break;
    }

    amxc_var_clean(&args);
    amxc_var_clean(&modifiedValues);

    return ret;
}
