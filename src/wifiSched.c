/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <assert.h>
#include <limits.h>

#include "debug/sahtrace.h"
#include "wifiSched.h"
#include "wifiSched_network.h"
#include "wifiSched_group.h"
#include "wifiSched_util.h"
#include "swl/fileOps/swl_fileUtils.h"

#define ME "wifisch"

typedef struct _wifiSched_app {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxb_bus_ctx_t* targetBusContext;
    unsigned int activeSchedulers;
    amxc_var_t groupActiveSchedulers;
} wifiSched_app_t;

const char* wifiSched_enableMethod_str[] = {"Parameter", "Function", "Unknown", "Max"};

/* hash table containing the number of active schedules
 * for a given group.
 * The key is a string containtng the group name
 * The value is an unsigned int containing the number of
 * active schedules
 */
wifiSched_app_t s_app;

amxd_dm_t* wifiSched_getDataModel(void) {
    return s_app.dm;
}

amxo_parser_t* wifiSched_getParser(void) {
    return s_app.parser;
}

amxc_var_t* wifiSched_getGroupHashTable(void) {
    return &s_app.groupActiveSchedulers;
}

unsigned int* wifiSched_getActiveSchedulers(void) {
    return &s_app.activeSchedulers;
}

amxb_bus_ctx_t* wifiSched_getTargetBusContext(void) {
    if(s_app.targetBusContext == NULL) {
        s_app.targetBusContext = amxb_be_who_has(
            amxd_object_get_value(cstring_t,
                                  amxd_dm_get_object(wifiSched_getDataModel(), "WiFiScheduler"),
                                  "GlobalTargetConfig",
                                  NULL));
    }
    return s_app.targetBusContext;
}

/*
 * Returns an enumeration value from the 'reason' parameter provided by the
 * scheduler data model
 */
wifiSched_scheduleReason_e s_getReasonFromString(const char* reason) {
    if(swl_str_matches(reason, "start")) {
        return SCHED_REASON_START;
    } else if(swl_str_matches(reason, "stop")) {
        return SCHED_REASON_STOP;
    } else {
        return SCHED_REASON_UNKNOWN;
    }
}

wifiSched_enableMethod_e s_getEnableMethodFromString(char* enableMethod) {
    return swl_conv_charToEnum(enableMethod, wifiSched_enableMethod_str, WIFI_SCHED_ENABLE_METHOD_MAX, WIFI_SCHED_ENABLE_METHOD_PARAM);
}

amxd_status_t _wifiSched_setGlobalTargetConfig_pwf(amxd_object_t* object,
                                                   amxd_param_t* param,
                                                   amxd_action_t reason,
                                                   const amxc_var_t* const args,
                                                   amxc_var_t* const retval,
                                                   void* priv) {

    int ret = amxd_action_param_write(object, param, reason, args, retval, priv);
    const char* targetDataModel = amxc_var_constcast(cstring_t, args);
    s_app.targetBusContext = amxb_be_who_has(targetDataModel);
    if(s_app.targetBusContext == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed obtaining the bus associated with %s", targetDataModel);
    }
    return ret;
}

amxd_status_t _wifiSched_scheduleCleanup_odf(amxd_object_t* object,
                                             amxd_param_t* param _UNUSED,
                                             amxd_action_t reason,
                                             const amxc_var_t* const args _UNUSED,
                                             amxc_var_t* const retval _UNUSED,
                                             void* priv _UNUSED) {

    amxp_scheduler_t* scheduler = NULL;

    if(reason != action_object_destroy) {
        return amxd_status_function_not_implemented;
    }

    if(amxd_object_get_type(object) != amxd_object_template) {
        return amxd_status_invalid_type;
    }

    scheduler = (amxp_scheduler_t*) object->priv;
    amxp_scheduler_delete(&scheduler);
    object->priv = NULL;

    return amxd_status_ok;
}

void _wifiSched_enableChanged_ehf(const char* const event_name _UNUSED,
                                  const amxc_var_t* const event_data,
                                  void* const priv _UNUSED) {
    ASSERT_NOT_NULL(event_data, , ME, "event_data is NULL");

    amxd_object_t* wifiSchedulerObject = amxd_dm_signal_get_object(wifiSched_getDataModel(), event_data);
    ASSERT_NOT_NULL(wifiSchedulerObject, , ME, "Failed getting the main WiFi Scheduler data model object");

    /*If the Enable value changed, the event_data variant will contain the new
       bool value under 'parameters.Enable.to'. If not, amxc_var_get_path returns NULL*/
    amxc_var_t* enableValueVariant = amxc_var_get_path(event_data, "parameters.Enable.to", AMXC_VAR_FLAG_DEFAULT);
    if(enableValueVariant == NULL) {
        //The enable value was not modified.
        return;
    }

    /*Iterate through the existing Network.Schedule instances and update them*/
    amxd_object_t* networkObject = amxd_object_get(wifiSchedulerObject, "Network");
    amxd_object_t* networkScheduleObject = amxd_object_get(networkObject, "Schedule");
    amxd_object_iterate(instance, it, networkScheduleObject) {
        amxd_object_t* scheduleInstance = amxc_container_of(it, amxd_object_t, it);
        amxp_scheduler_t* scheduler = (amxp_scheduler_t*) networkScheduleObject->priv;
        wifiSched_util_updateScheduler(wifiSchedulerObject, networkScheduleObject, scheduleInstance, scheduler, wifiSched_network_scheduleTriggered_cbf);
    }

    /*Iterate through the existing Group.*.Schedule instances and update them*/
    amxd_object_t* groupObject = amxd_object_get(wifiSchedulerObject, "Group");
    amxd_object_iterate(instance, groupIt, groupObject) {
        amxd_object_t* groupInstance = amxc_container_of(groupIt, amxd_object_t, it);
        amxd_object_t* groupScheduleObject = amxd_object_get(groupInstance, "Schedule");
        amxd_object_iterate(instance, scheduleIt, groupScheduleObject) {
            amxd_object_t* scheduleInstance = amxc_container_of(scheduleIt, amxd_object_t, it);
            amxp_scheduler_t* scheduler = (amxp_scheduler_t*) groupScheduleObject->priv;
            wifiSched_util_updateScheduler(wifiSchedulerObject, groupScheduleObject, scheduleInstance, scheduler, wifiSched_group_scheduleTriggered_cbf);
        }
    }
}

/*
 * Main entry point
 */
int _wifiSched_main(int reason,
                    amxd_dm_t* dm,
                    amxo_parser_t* parser) {
    switch(reason) {
    case 0:     // START
        SAH_TRACEZ_INFO(ME, "wifi-scheduler started");
        s_app.dm = dm;
        s_app.parser = parser;
        s_app.activeSchedulers = 0;

        amxc_var_init(wifiSched_getGroupHashTable());
        amxc_var_set_type(wifiSched_getGroupHashTable(), AMXC_VAR_ID_HTABLE);

        break;
    case 1:     // STOP
        SAH_TRACEZ_INFO(ME, "wifi-scheduler stopped");
        s_app.dm = NULL;
        s_app.parser = parser;
        amxc_var_clean(wifiSched_getGroupHashTable());
        break;
    }

    return 0;
}
