include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 odl/wifi-scheduler.odl $(DEST)/etc/amx/wifi-scheduler/wifi-scheduler.odl
	$(INSTALL) -D -p -m 0644 odl/wifi-scheduler_definition.odl $(DEST)/etc/amx/wifi-scheduler/wifi-scheduler_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-wifi-scheduler_mapping.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/wifi-scheduler/defaults
	$(INSTALL) -D -p -m 0644 odl/defaults/* $(DEST)/etc/amx/wifi-scheduler/defaults/
	$(INSTALL) -D -p -m 0755 src/wifi-scheduler.so $(DEST)/usr/lib/amx/wifi-scheduler/wifi-scheduler.so
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/wifi-scheduler
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json

package: all
	$(INSTALL) -D -p -m 0644 odl/wifi-scheduler.odl $(PKGDIR)/etc/amx/wifi-scheduler/wifi-scheduler.odl
	$(INSTALL) -D -p -m 0644 odl/wifi-scheduler_definition.odl $(PKGDIR)/etc/amx/wifi-scheduler/wifi-scheduler_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-wifi-scheduler_mapping.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/wifi-scheduler/defaults
	$(INSTALL) -D -p -m 0644 odl/defaults/* $(PKGDIR)/etc/amx/wifi-scheduler/defaults/
	$(INSTALL) -D -p -m 0755 src/wifi-scheduler.so $(PKGDIR)/usr/lib/amx/wifi-scheduler/wifi-scheduler.so
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/wifi-scheduler
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/wifi-scheduler
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(MAKE) -C doc doc

	$(eval ODLFILES += odl/wifi-scheduler.odl)
	$(eval ODLFILES += odl/wifi-scheduler_definition.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_mapping.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C src all
	$(MAKE) -C test report
	$(MAKE) -C test coverage_report

.PHONY: all clean changelog install package doc test